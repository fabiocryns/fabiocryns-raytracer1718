; Listing generated by Microsoft (R) Optimizing Compiler Version 19.11.25508.2 

include listing.inc

INCLUDELIB MSVCRT
INCLUDELIB OLDNAMES

PUBLIC	??0QuadraticEquation@math@@QEAA@NNN@Z		; math::QuadraticEquation::QuadraticEquation
PUBLIC	?has_solutions@QuadraticEquation@math@@QEBA_NXZ	; math::QuadraticEquation::has_solutions
PUBLIC	?x1@QuadraticEquation@math@@QEBANXZ		; math::QuadraticEquation::x1
PUBLIC	?x2@QuadraticEquation@math@@QEBANXZ		; math::QuadraticEquation::x2
PUBLIC	__real@3fe0000000000000
PUBLIC	__real@4010000000000000
PUBLIC	__xmm@80000000000000008000000000000000
EXTRN	_fltused:DWORD
;	COMDAT pdata
pdata	SEGMENT
$pdata$??0QuadraticEquation@math@@QEAA@NNN@Z DD imagerel $LN6
	DD	imagerel $LN6+132
	DD	imagerel $unwind$??0QuadraticEquation@math@@QEAA@NNN@Z
pdata	ENDS
;	COMDAT __xmm@80000000000000008000000000000000
CONST	SEGMENT
__xmm@80000000000000008000000000000000 DB 00H, 00H, 00H, 00H, 00H, 00H, 00H
	DB	080H, 00H, 00H, 00H, 00H, 00H, 00H, 00H, 080H
CONST	ENDS
;	COMDAT __real@4010000000000000
CONST	SEGMENT
__real@4010000000000000 DQ 04010000000000000r	; 4
CONST	ENDS
;	COMDAT __real@3fe0000000000000
CONST	SEGMENT
__real@3fe0000000000000 DQ 03fe0000000000000r	; 0.5
CONST	ENDS
;	COMDAT xdata
xdata	SEGMENT
$unwind$??0QuadraticEquation@math@@QEAA@NNN@Z DD 030b01H
	DD	0680bH
	DD	02204H
xdata	ENDS
; Function compile flags: /Ogtpy
; File c:\users\fabiocryns\source\repos\fabiocryns-raytracer1718\raytracer\raytracer\math\quadratic_equation.cpp
;	COMDAT ?x2@QuadraticEquation@math@@QEBANXZ
_TEXT	SEGMENT
this$ = 8
?x2@QuadraticEquation@math@@QEBANXZ PROC		; math::QuadraticEquation::x2, COMDAT

; 42   :     assert(has_solutions());
; 43   : 
; 44   :     return m_x2;

	movsd	xmm0, QWORD PTR [rcx+8]

; 45   : }

	ret	0
?x2@QuadraticEquation@math@@QEBANXZ ENDP		; math::QuadraticEquation::x2
_TEXT	ENDS
; Function compile flags: /Ogtpy
; File c:\users\fabiocryns\source\repos\fabiocryns-raytracer1718\raytracer\raytracer\math\quadratic_equation.cpp
;	COMDAT ?x1@QuadraticEquation@math@@QEBANXZ
_TEXT	SEGMENT
this$ = 8
?x1@QuadraticEquation@math@@QEBANXZ PROC		; math::QuadraticEquation::x1, COMDAT

; 35   :     assert(has_solutions());
; 36   : 
; 37   :     return m_x1;

	movsd	xmm0, QWORD PTR [rcx]

; 38   : }

	ret	0
?x1@QuadraticEquation@math@@QEBANXZ ENDP		; math::QuadraticEquation::x1
_TEXT	ENDS
; Function compile flags: /Ogtpy
; File c:\users\fabiocryns\source\repos\fabiocryns-raytracer1718\raytracer\raytracer\math\quadratic_equation.cpp
;	COMDAT ?has_solutions@QuadraticEquation@math@@QEBA_NXZ
_TEXT	SEGMENT
this$ = 8
?has_solutions@QuadraticEquation@math@@QEBA_NXZ PROC	; math::QuadraticEquation::has_solutions, COMDAT

; 30   :     return m_has_solutions;

	movzx	eax, BYTE PTR [rcx+16]

; 31   : }

	ret	0
?has_solutions@QuadraticEquation@math@@QEBA_NXZ ENDP	; math::QuadraticEquation::has_solutions
_TEXT	ENDS
; Function compile flags: /Ogtpy
; File c:\users\fabiocryns\source\repos\fabiocryns-raytracer1718\raytracer\raytracer\math\quadratic_equation.cpp
;	COMDAT ??0QuadraticEquation@math@@QEAA@NNN@Z
_TEXT	SEGMENT
this$ = 32
a$ = 40
b$ = 48
c$ = 56
??0QuadraticEquation@math@@QEAA@NNN@Z PROC		; math::QuadraticEquation::QuadraticEquation, COMDAT

; 8    : {

$LN6:
	sub	rsp, 24

; 9    :     assert(a != approx(0.0));
; 10   : 
; 11   :     double discriminant = b * b - 4 * a * c;

	movaps	xmm0, xmm1
	movaps	XMMWORD PTR [rsp], xmm6
	mulsd	xmm0, QWORD PTR __real@4010000000000000
	movaps	xmm4, xmm2

; 24   :         m_x2 = (-b + sqrt_d) / (2 * a);
; 25   :     }
; 26   : }

	mov	rax, rcx
	mulsd	xmm4, xmm2
	movaps	xmm5, xmm2
	movaps	xmm6, xmm1
	mulsd	xmm0, xmm3
	subsd	xmm4, xmm0
	xorps	xmm0, xmm0
	comisd	xmm4, xmm0
	jae	SHORT $LN2@QuadraticE

; 12   : 
; 13   :     if (discriminant < 0)
; 14   :     {
; 15   :         m_has_solutions = false;

	mov	BYTE PTR [rcx+16], 0

; 24   :         m_x2 = (-b + sqrt_d) / (2 * a);
; 25   :     }
; 26   : }

	movaps	xmm6, XMMWORD PTR [rsp]
	add	rsp, 24
	ret	0
$LN2@QuadraticE:

; 16   :     }
; 17   :     else
; 18   :     {
; 19   :         m_has_solutions = true;
; 20   : 
; 21   :         double sqrt_d = sqrt(discriminant);
; 22   : 
; 23   :         m_x1 = (-b - sqrt_d) / (2 * a);

	movsd	xmm1, QWORD PTR __real@3fe0000000000000
	xorps	xmm2, xmm2
	sqrtsd	xmm2, xmm4
	movaps	xmm0, xmm5
	mov	BYTE PTR [rcx+16], 1
	xorps	xmm0, QWORD PTR __xmm@80000000000000008000000000000000
	divsd	xmm1, xmm6

; 24   :         m_x2 = (-b + sqrt_d) / (2 * a);
; 25   :     }
; 26   : }

	movaps	xmm6, XMMWORD PTR [rsp]
	subsd	xmm0, xmm2
	subsd	xmm2, xmm5
	mulsd	xmm0, xmm1
	mulsd	xmm2, xmm1
	movsd	QWORD PTR [rcx], xmm0
	movsd	QWORD PTR [rcx+8], xmm2
	add	rsp, 24
	ret	0
??0QuadraticEquation@math@@QEAA@NNN@Z ENDP		; math::QuadraticEquation::QuadraticEquation
_TEXT	ENDS
END
