; Listing generated by Microsoft (R) Optimizing Compiler Version 19.11.25508.2 

include listing.inc

INCLUDELIB MSVCRT
INCLUDELIB OLDNAMES

PUBLIC	??0LightRay@raytracer@@QEAA@AEBURay@math@@AEBUColor@imaging@@@Z ; raytracer::LightRay::LightRay
; Function compile flags: /Ogtpy
; File c:\users\fabiocryns\source\repos\fabiocryns-raytracer1718\raytracer\raytracer\lights\light-ray.cpp
;	COMDAT ??0LightRay@raytracer@@QEAA@AEBURay@math@@AEBUColor@imaging@@@Z
_TEXT	SEGMENT
this$ = 8
ray$ = 16
color$ = 24
??0LightRay@raytracer@@QEAA@AEBURay@math@@AEBUColor@imaging@@@Z PROC ; raytracer::LightRay::LightRay, COMDAT

; 4    :     : ray(ray), color(color)

	movups	xmm0, XMMWORD PTR [rdx]

; 6    :     // NOP
; 7    : }

	mov	rax, rcx
	movups	XMMWORD PTR [rcx], xmm0
	movups	xmm1, XMMWORD PTR [rdx+16]
	movups	XMMWORD PTR [rcx+16], xmm1
	movups	xmm0, XMMWORD PTR [rdx+32]
	movups	XMMWORD PTR [rcx+32], xmm0
	movups	xmm0, XMMWORD PTR [r8]
	movups	XMMWORD PTR [rcx+48], xmm0
	movsd	xmm1, QWORD PTR [r8+16]
	movsd	QWORD PTR [rcx+64], xmm1
	ret	0
??0LightRay@raytracer@@QEAA@AEBURay@math@@AEBUColor@imaging@@@Z ENDP ; raytracer::LightRay::LightRay
_TEXT	ENDS
END
