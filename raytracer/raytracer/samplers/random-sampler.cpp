#include "samplers/single-sampler.h"
#include "random-sampler.h"
#include <random>

using namespace math;
using namespace raytracer;


namespace
{
	class RandomSampler : public raytracer::samplers::_private_::SamplerImplementation
	{
	public:
		int m_sample_count;

		std::vector<math::Point2D> sample(const math::Rectangle2D& rectangle) const override
		{
			std::uniform_real_distribution<> dis(0, 1);
			std::random_device rd;
			std::mt19937 gen(rd()); 
			std::vector<math::Point2D> points;
			for (int n = 0; n < m_sample_count; ++n) {
				points.push_back(rectangle.from_relative(math::Point2D(dis(gen), dis(gen))));
			}
			return points;
		}

		RandomSampler(int sample_count) {
			m_sample_count = sample_count;
		}
	};
}

Sampler raytracer::samplers::random(int sample_count)
{
	return Sampler(std::make_shared<RandomSampler>(sample_count));
}
