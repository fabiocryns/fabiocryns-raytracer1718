#pragma once

#include "samplers/sampler.h"

namespace raytracer
{
	namespace samplers
	{
		/// <summary>
		/// Random Sampler.
		/// </summary>
		Sampler random(int sample_count);
	}
}